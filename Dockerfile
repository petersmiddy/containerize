FROM python:3.5-alpine3.8

# TODO Set metadata for who maintains this image
LABEL maintainer="peter.smiddy@libertymutual.com"

COPY * /app/
RUN pip3 install Flask
EXPOSE 8080

#TODO Set default values for env variables
ENV DISPLAY_FONT='arial'
ENV DISPLAY_COLOR='green'
ENV ENVIRONMENT='Dev'

#TODO *bonus* add a health check that tells docker the app is running properly
RUN apk add curl
HEALTHCHECK CMD curl --fail http://localhost:8080/ || exit 1

# TODO have the app run as a non-root user
USER 1001

CMD python /app/app.py

