import os
#import configparser
from flask import Flask, redirect, request, url_for
import logging
import sys

root = logging.getLogger()
root.setLevel(logging.DEBUG)

handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
root.addHandler(handler)

#Setting default envronment variables with os module
os.environ['DISPLAY_FONT'] = 'arial'
os.environ['ENVIRONMENT'] = 'Prod'
os.environ['DISPLAY_COLOR'] = 'green'

#Retrieving default environment variables with os module
font = os.environ.get('DISPLAY_FONT')
font_color = os.environ.get('DISPLAY_COLOR')
environment = os.environ.get('ENVIRONMENT')

#config = configparser.ConfigParser()
#has_config = config.read('vars.ini')


# configurations
#font = config['style']['DISPLAY_FONT']
#font_color = config['style']['DISPLAY_COLOR']
#environment = config['debug']['ENVIRONMENT']

signatures = []

app = Flask(__name__)

@app.route('/', methods=['GET'])
def index():
    html = """
    <style>
    body { background-color: #C0C0C0;
         }
    </style>
    <h1 style='text-align: center; border: 2px solid black;'>Signatures:</h1>
    <br />
    <font face="%(font)s" color="%(color)s">
        %(messages)s
    </font>

    <br /> <br />
    <form action="/signatures" method="post">
        Sign the Guestbook: <input type="text" name="message"><br>
        <input type="submit" value="Sign">
    </form>
    
    <div class="shadow-lg p-3 mb-5 bg-white rounded"> This guestbook was made using the power of Flask! </div>

    <img src="https://cdn-media-1.freecodecamp.org/images/1SnE5y1jhzqsSoFvgFyWc4mRLXX-iuG2DPtm" class="kg-image">

    <br />
    <br />
    Debug Info: <br />
    ENVIRONMENT is %(environment)s
    """
    messages_html = "<br />".join(signatures)
    return html % {"font": font, "color": font_color, "messages": messages_html, "environment": environment}

@app.route('/signatures', methods=['POST'])
def write():
    message = request.form.get('message')
    signatures.append(message)

    return redirect(url_for('index'))

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080)

